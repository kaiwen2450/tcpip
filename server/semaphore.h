#include "sem.h"

void rvobj(int semid)
{
  if (semctl(semid, 0, IPC_RMID, NULL) == -1)
    perror("\nsemctl fails");
}

int initsem(key_t semkey)
{
  int status = 0, semid;
  semun arg;

  if ((semid = semget(semkey,1, SEMPERM | IPC_CREAT | IPC_EXCL)) == -1)
  {
    if (errno == EEXIST) /* if file exists  */
      semid = semget(semkey, 1, 0);
  } /* to access the semaphore */
  else
  {
    arg.val = 1;
    status = semctl(semid, 0, SETVAL, arg);
  }

  if (semid == -1 || status == -1)
  {
    perror("Initsem() fails");
    return (-1);
  }
  return (semid);
}

int p(int semid)
{
  struct sembuf p_buf;
  p_buf.sem_num = 0; /* this set only contains 1 semaphore  */
  p_buf.sem_op = -1;
  p_buf.sem_flg = SEM_UNDO; /* undo all the operations on semaphore when process exits */
  if (semop(semid, &p_buf, 1) == -1)
  { /* perform the fundamental semaphore operations p(), decrement semaphore to lock semaphore */
    perror("P () fails");
    exit(1);
  }
  return (0);
}

int v(int semid)
{
  struct sembuf v_buf;
  v_buf.sem_num = 0; /* this set only contains 1 semaphore  */
  v_buf.sem_op = 1;
  v_buf.sem_flg = SEM_UNDO; /* undo all the operations on semaphore when process exits */
  if (semop(semid, &v_buf, 1) == -1)
  { /* perform the fundamental semaphore operations v(), increment semaphore to unlock semaphore */
    perror("V () fails");
    exit(1);
  }
  return (0);
}
