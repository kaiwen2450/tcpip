#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//aditional libraries for actions
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>

#define SERVER_PORT 25001
#define BUFFSIZE 1024
//file permission for actions
#define PERM 0666
