#include "inet.h"
#include <signal.h>
#include "actions.h"
#include "semaphore.h"

int main()
{
    fd_set master;
    fd_set read_fds;
    int fdmax, sockfd, new_sockfd, nbytes, yes = 1, addrlen, i, j;
    char buffer[BUFFSIZE + 1];
    struct sockaddr_in serv_addr, cli_addr;

    key_t semkey = 0x200;
    int semid, recv_gate;

    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGTSTP);
    sigprocmask(SIG_BLOCK, &set, NULL);

    FD_ZERO(&master);
    FD_ZERO(&read_fds);

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("Server: socket() error\n");
        exit(1);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
    {
        printf("\nSetsockopt() error!!!\n");
        exit(1);
    }

    printf("---Welcome joining email system---\n");
    // TCP connection setup
    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(SERVER_PORT);
    bzero(&(serv_addr.sin_zero), 8);

    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("Server: bind() error\n");
        exit(1);
    }

    //Waiting for client bind the connection
    printf("\nWaiting for connection... [bind]\n");
    if (listen(sockfd, 5) < 0)
    { //Only five user are allowed to connect in the same time
        printf("failed to listen!!!\n");
        exit(1);
    }

    FD_SET(sockfd, &master);
    fdmax = sockfd;

    for (;;) //while(1)
    {
        read_fds = master;

        if (select(fdmax + 1, &read_fds, NULL, NULL, NULL) == -1)
        {
            printf("Select() error\n");
            exit(1);
        }
        for (int i = 0; i <= fdmax; i++)
        {
            if (FD_ISSET(i, &read_fds))
            {
                if (i == sockfd)
                {
                    addrlen = sizeof(cli_addr);
                    if ((new_sockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &addrlen)) == -1)
                    {
                        printf("\naccept() error!!!!\n");
                    }
                    else
                    {
                        FD_SET(new_sockfd, &master);
                        if (new_sockfd > fdmax)
                        {
                            fdmax = new_sockfd;
                        }
                        printf("selector server: new connection from %s on socket %d \n", inet_ntoa(cli_addr.sin_addr), new_sockfd);
                    }
                }
                else
                {
                    bzero(buffer, sizeof(buffer));
                    recv_gate = (nbytes = recv(i, buffer, sizeof(buffer), 0));
                    //create a critical section and block other incoming buffer until the action is done
                    if (recv_gate <= 0)
                    {
                        if (nbytes == 0)
                        {
                            printf("select server: socket %d hung up \n", i);
                        }
                        else
                        {
                            printf("\nrecv error!!!\n");
                        }
                        close(i);
                        FD_CLR(i, &master);
                    }
                    else
                    {
                        if ((semid = initsem(semkey)) < 0)
                        {
                            perror("failed to init semaphore\n");
                            exit(1);
                        }
                        p(semid);
                        printf("Message [ %s ] from socket [ %d ] client [ %s ] \n", buffer, i, inet_ntoa(cli_addr.sin_addr));
                        int counter = 0; //instantiate a counter to assign every elements into one array
                        char *element = strtok(buffer, "/");
                        char *array[3];
                        char buffer1[BUFFSIZE + 1];

                        while (element != NULL)
                        {
                            array[counter++] = element;
                            element = strtok(NULL, "/");
                        }

                        //Starting to collect action here, insert action such as login and register here for further action
                        if (strcmp(array[0], "login") == 0)
                        {
                            login(i, array[1]);
                        }

                        else if (strcmp(array[0], "register") == 0)
                        {
                            registerClient(i, array[1]);
                        }

                        else if (strcmp(array[1], "inbox") == 0)
                        {
                            readmsg(i, array[0], array[2]); //array 0 is client username, array 2 here contain string "read" or "unread"
                        }

                        else if (strcmp(array[0], "compose") == 0)
                        {
                            writemsg(i, array[1]);
                        }

                        else if (strcmp(array[0], "exit") == 0)
                        {
                            printf("socket %d is leaving \n", i);
                        }
                        else
                        {
                            printf("Command not found!\n");
                            bzero(buffer, sizeof(buffer));
                        }
                    }
                    v(semid);
                    //action done and release process
                }
            }
        }
    }
    rvobj(semid);
}
