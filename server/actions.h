#define FALSE 0
#define TRUE 1

int isExist(char *username);
//Inbox
void readmsg(int sockfd, char username[], char type[])
{
	int fd, nread = 0;
	char filename[100] = "data/", filename2[100] = "data/";
	char readbuff[BUFFSIZE + 1] = "";
	char temp[BUFFSIZE];
	char temp_read[BUFFSIZE + 1];
	char *array[BUFFSIZE];
	strcat(filename, username);
	strcat(filename2, username);

	printf("\nReceived reading request [%s] request from CLIENT [%s]\n", type, username);

	if (strcmp(type, "read") == 0)
	{
		strcat(filename, "_read.txt");
	}
	else if (strcmp(type, "unread") == 0)
	{
		strcat(filename, "_unread.txt");
		strcat(filename2, "_read.txt");
	}
	fd = open(filename, O_RDWR | O_CREAT, PERM);
	if (fd < 0)
	{
		perror("failed to open\n");
		exit(1);
	}
	while (read(fd, &readbuff[nread], 1) == 1)
	{
		temp[nread] = readbuff[nread];
		if (readbuff[nread] == '\n' || readbuff[nread] == 0x0)
		{ //escape endline
			readbuff[nread] = 0;
			continue;
		}
		nread++;
	}

	//write the buffer receive into a text structure for better
	char *element = strtok(temp, ";");
	int counter = 0;
	while (element != NULL)
	{
		array[counter++] = element;
		element = strtok(NULL, ";");
	}
	bzero(temp_read, sizeof(temp_read));
	for (int i = 0; i < counter - 1; i++)
	{
		strcat(temp_read, array[i]);
		strcat(temp_read, ";");
		if ((i - 3) % 4 == 0 && i > 0)
		{
			strcat(temp_read, "\n");
		}
	}

	if ((strcmp(type, "unread") == 0) && nread > 0)
	{
		int fd2 = open(filename2, O_RDWR | O_CREAT | O_APPEND, PERM);
		if (write(fd2, temp_read, strlen(temp_read)) < 0)
		{
			close(fd2);
			printf("failed to write to %s_read.txt\n", username);
			exit(2);
		}
		if (remove(filename) != 0)
		{
			printf("failed to remove %s_unread.txt\n", username);
			exit(2);
		}
		printf("write to read %s", filename2);
		close(fd2);
	}
	//send readbuff to client
	printf("\nSending back message to CLIENT\n");
	if (nread == 0)
		send(sockfd, "No message", strlen("No message"), 0);
	else
		send(sockfd, readbuff, BUFFSIZE, 0);

	bzero(readbuff, sizeof(readbuff));
	close(fd);
}
//Compose
void writemsg(int sockfd, char *buffer)
{
	char message[BUFFSIZE];
	char temp[BUFFSIZE + 1];
	int fd[2];

	//save in "To" username file
	strcpy(temp, buffer);
	char filename[100] = "data/";
	char *toUser = strtok(temp, ";");
	strcat(filename, toUser);
	//check whether toUser name exists in account.txt
	if (isExist(toUser) > 0)
	{
		memset(temp, 0, sizeof(temp));
		strcat(filename, "_unread.txt");

		fd[0] = open(filename, O_RDWR | O_CREAT, PERM);
		if (fd[0] < 0)
		{
			strcpy(message, "Error writing in file!\n");
		}
		strcpy(temp, buffer);
		strcat(temp, "\n");
		lseek(fd[0], 0, SEEK_END);
		if (write(fd[0], temp, strlen(temp)) < 0)
		{
			strcpy(message, "Failed to open server file!\n");
		}
		strcpy(message, "Email Successfully sent!\n");
		printf("Email written in %s\n", filename);
		send(sockfd, message, BUFFSIZE, 0); //send notification to client whether it is successful or not
		bzero(message, sizeof(message));
		close(fd[0]);
	}
	else
	{
		strcpy(message, "Failed: Receiver user does not exist!\n");
		send(sockfd, message, BUFFSIZE, 0); //send notification to client whether it is successful or not
		bzero(message, sizeof(message));
	}
}
//Register
void registerClient(int sockfd, char *buffer)
{ //buffer contains new username and new password
	const char *filename = "data/account.txt";
	char message[BUFFSIZE];
	char temp[BUFFSIZE + 1];

	strcpy(temp, buffer);
	strcat(buffer, "\n");
	char *newUser = strtok(temp, ";"); //get new username

	int fd = open(filename, O_RDWR | O_CREAT | O_APPEND, PERM);
	if (fd < 0)
	{
		perror("failed to open account.txt\n");
		exit(1);
	}

	if (isExist(newUser) > 0)
	{
		strcpy(message, "Username is registerd. Please enter another username.\n");
		send(sockfd, message, BUFFSIZE, 0);
		bzero(message, sizeof(message));
	}
	else
	{
		if (write(fd, buffer, strlen(buffer)) < strlen(buffer))
		{
			close(fd);
			printf("failed to write to account.txt\n");
			exit(2);
		}
		strcpy(message, "Account is registerd successfully.\n");
		send(sockfd, message, BUFFSIZE, 0);
		bzero(message, sizeof(message));
	}
	close(fd);
}
//Login
void login(int sockfd, char *buffer)
{
	const char *filename = "data/account.txt";
	char readbuff[BUFFSIZE + 1] = "";
	char temp[BUFFSIZE + 1];
	char message[BUFFSIZE];
	int nread = 0;
	int isLogged = 0; // 0 - not logged, 1 - logged

	char *username = strtok(buffer, ";"); //get username
	char *password = strtok(NULL, ";");	  //get password

	/*
	strtok phase 1: <username>;<password>; --> <username>
	strtok phase 2: <password>; --> <password>
	*/

	int fd = open(filename, O_RDONLY | O_CREAT, PERM);
	if (fd < 0)
	{
		perror("failed to open account.txt\n");
		exit(1);
	}
	while (read(fd, &readbuff[nread], 1) == 1)
	{
		if (readbuff[nread] == '\n' || readbuff[nread] == 0x0)
		{ //escape endline
			readbuff[nread] = 0;
			continue;
		}
		nread++;
	}

	char *pair = strtok(readbuff, ";");
	while (pair != NULL)
	{
		if (strcmp(pair, username) == 0)
		{
			char *p = strtok(NULL, ";");
			if (strcmp(p, password) == 0)
			{
				isLogged = 1;
			}
		}
		pair = strtok(NULL, ";");
	}

	if (isLogged == 1)
	{
		strcpy(message, "Logged");
		send(sockfd, message, BUFFSIZE, 0);
		bzero(message, sizeof(message));
	}
	else
	{
		strcpy(message, "Not logged");
		send(sockfd, message, BUFFSIZE, 0);
		bzero(message, sizeof(message));
	}
	close(fd);
}

int isExist(char *username)
{ // 0 - not exist, 1 - exists
	int count = 0;
	char readbuff[BUFFSIZE + 1] = "";
	int nread = 0;
	char *filename = "data/account.txt";
	int fd = open(filename, O_RDWR | O_CREAT | O_APPEND, PERM);
	if (fd < 0)
	{
		perror("failed to open account.txt\n");
		exit(1);
	}
	while (read(fd, &readbuff[nread], 1) == 1)
	{
		if (readbuff[nread] == '\n' || readbuff[nread] == 0x0)
		{ //escape endline
			readbuff[nread] = 0;
			continue;
		}
		nread++;
	}

	char *row = strtok(readbuff, ";");
	while (row != NULL)
	{
		if ((strcmp(username, row)) == 0 && count % 2 == 0)
		{
			close(fd);
			return 1;
		}
		row = strtok(NULL, ";");
		count++;
	}
	close(fd);
	return 0;
}
