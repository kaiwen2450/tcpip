#include "inet.h"


void emailSystem(int sockfd);
void login(int sockfd);
void registerAcc(int sockfd);
void mainMenu(int sockfd, char userName[]);
void printInbox(char string[]);
void compose(int sockfd, char userName[]);
char *getDate();
int quit = 0; // 0 - true, 1 - false

struct message
{
    long mtype;
    char message_text[BUFFSIZE + 1];
} message;

int main(int argc, char *argv[])
{ //To obtain the parameter input from terminal
    int sockfd, new_sockfd, clilen, check;
    char buffer[BUFFSIZE];
    struct sockaddr_in serv_addr;
    struct sockaddr_in cli_addr;


    if (argc <= 1)
    { //If there is no parameter included from terminal, line will be printed as a guide
        printf("How to use : %s remoteIPaddress [example: ./client 127.0.0.1\n", argv[0]);
        exit(1);
    }

    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(SERVER_PORT);
    inet_pton(AF_INET, argv[1], &serv_addr.sin_addr);

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("Client: socket() error\n");
        exit(1);
    }

    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("Client: connect() error\n");
        exit(1);
    }
    switch(fork()){
      case -1:
        perror("failed to fork\n");
        exit(1);
        break;
      case 0: //child process
        //This is the part where we start own system implementation
        
        while (quit != 1)
        {
            emailSystem(sockfd);
        }
        
        break;
      default: 
        wait((int *)0);
        close(sockfd);
        printf("Closing program [pid: %d]...\n", getpid());
        break;
    }
}

void emailSystem(int sockfd)
{
    int option;
    char username[BUFFSIZE + 1], password[BUFFSIZE + 1];
    char temp[BUFFSIZE];
    char receiver[BUFFSIZE];
    char content[BUFFSIZE];
    printf("\n--- Welcome to email system ---\nSelect the following options:\n1.Login\n2.Register\n3.Exit\n\nYour select: ");
    scanf("%d", &option);
    switch (option)
    {
    case 1:
        login(sockfd);
        break;
    case 2:
        registerAcc(sockfd);
        break;

    case 3:
        send(sockfd, "exit/", 6, 0);
        printf("Terminating communication...\n");
        quit = 1;
        break;

    default:
        printf("Error input!!!\n");
        break;
    }
}

void login(int sockfd)
{
    char buffer[BUFFSIZE + 1] = "login/";
    char username[BUFFSIZE], password[BUFFSIZE];
    printf("--- Login ---\n");
    printf("Username: ");
    scanf("%s", username);
    strcat(buffer, username);
    strcat(buffer, ";");
    printf("Password: ");
    scanf("%s", password);
    strcat(buffer, password);

    send(sockfd, buffer, BUFFSIZE, 0);
    bzero(buffer, sizeof(buffer));
    recv(sockfd, buffer, BUFFSIZE, 0);
    if (strcmp(buffer, "Logged") == 0)
    {
        printf("Success login!\n\n");
        //Start Menu here
        while (quit != 1)
            mainMenu(sockfd, username);
    }
    else
    {
        printf("Invalid username and/or password.\n");
    }
}

void registerAcc(int sockfd)
{
    char buffer[BUFFSIZE + 1] = "register/";
    char username[BUFFSIZE], password[BUFFSIZE];
    printf("--- Register Account ---\n");
    printf("New Username: ");
    scanf("%s", username);
    strcat(buffer, username);
    strcat(buffer, ";");
    printf("New Password: ");
    scanf("%s", password);
    strcat(buffer, password);
    strcat(buffer, ";");

    send(sockfd, buffer, BUFFSIZE, 0);
    bzero(buffer, sizeof(buffer));
    recv(sockfd, buffer, BUFFSIZE, 0);
    printf("Register Status: %s\n\n", buffer);
}

void mainMenu(int sockfd, char userName[])
{
    char buffer[BUFFSIZE + 1];
    int opt, opt2;
    char category[20], receiver[BUFFSIZE + 1], content[BUFFSIZE];

    printf("--- MailBox ---\n1. Inbox\n2. Compose Mail\n3. Exit\n\nYour select: ");
    scanf("%d", &opt);
    switch (opt)
    {
    case 1: //reading
        printf("--- Inbox ---\n1. read\n2. unread\n\n");
        scanf("%d", &opt2);
        strcpy(category, userName);
        strcat(category, "/inbox/");
        if (opt2 == 1)
        {
            strcat(category, "read");
        }
        else if (opt2 == 2)
        {
            strcat(category, "unread");
        }
        else
        {
            printf("Invalid input.\n");
        }
        send(sockfd, category, strlen(category), 0);
        bzero(buffer, sizeof(buffer));
        recv(sockfd, buffer, BUFFSIZE, 0);
        printInbox(buffer);
        //print messages in organized way
        break;
    case 2:
        compose(sockfd, userName);
        break;
    case 3:
        send(sockfd, "exit/", 6, 0);
        close(sockfd);
        quit = 1;
        break;
    default:
        printf("Invalid input\n");
        break;
    }
}

void printInbox(char string[])
{
    char *row = strtok(string, ";");
    int i = 0, line = 0;

    printf("No\t\tTo\t\tFrom\t\tMessage\t\tDate\n");
    printf("------------------------------------------------------------------\n");
    while (row != NULL)
    {
        if (i % 4 == 0)
        {
            line++;
            printf("\n%d\t\t", line);
        }
        printf("%s\t\t", row);
        row = strtok(NULL, ";");
        i++;
    }
    printf("\n\n");
}

void compose(int sockfd, char userName[])
{
    char clean;
    char buffer[BUFFSIZE + 1], receiver[BUFFSIZE + 1], content[BUFFSIZE + 1];
    //child process got error have take a look a while
    int msqid = msgget(IPC_PRIVATE, IPC_CREAT | PERM);
    if (msqid == -1)
    {
        perror("failed on getting msqid!\n");
        exit(1);
    }

    pid_t pid = fork();
    if (pid == 0)
    {
        //child
        message.mtype = 1;
        char temp[BUFFSIZE];
        memset(&(message.message_text), 0, BUFFSIZE * sizeof(char));
        strcat(userName, ";");
        system("clear");
        printf("Receiver Email: ");
        scanf("%s", receiver);
        strcat(receiver, ";");
        printf("Content:  ");
        scanf("%c", &clean);
        scanf("%[^\n]", content); 
        strcat(content, ";");
        strcpy(temp, "compose/");
        strcat(temp, receiver);
        strcat(temp, userName);
        strcat(temp, content);
        strcat(temp, getDate());
        strcat(temp, ";"); //Buffer will be "Compose/<receiver>;<sender>;<content>;<Date>" and send to server for file manipulation

        strcpy(message.message_text, temp);
        if (msgsnd(msqid, &message, sizeof(message.message_text), 0) == -1)
        {
            perror("failed on send message queue!\n");
            exit(2);
        }
        exit(0); //end child process
    }
    else
    {
        //parent
        waitpid(pid, NULL, 0); //wait child process finish
        if (msgrcv(msqid, &message, sizeof(message), 1, 0) == -1)
        {
            perror("failed on receive message queue!\n");
            exit(2);
        }
        send(sockfd, message.message_text, BUFFSIZE, 0);
        if (msgctl(msqid, IPC_RMID, NULL) == -1)
        {
            perror("failed to remove message queue!\n");
            exit(2);
        }
        bzero(buffer,sizeof(buffer));
        recv(sockfd, buffer, BUFFSIZE, 0); //Determine whether the file is accepted or not
        printf("%s\n",buffer);
        printf("Heading back to main menu ....\n");
        sleep(3);
        mainMenu(sockfd, userName);
    }
}

char *getDate()
{
    // variables to store date and time components
    int hours, minutes, seconds, day, month, year;

    // time_t is arithmetic time type
    time_t now;

    // Obtain current time
    // time() returns the current time of the system as a time_t value
    time(&now);

    struct tm *local = localtime(&now);

    char date[50], time[50];
    char *dateNtime;
    dateNtime = malloc(sizeof(char) * 100);

    strftime(date, sizeof(date), "%d-%m-%y", local);
    strftime(time, sizeof(time), "%R%p", local);

    strcpy(dateNtime, date);
    strcat(dateNtime, " ");
    strcat(dateNtime, time);
    printf("date: %s\n", dateNtime);
    return dateNtime;
}
