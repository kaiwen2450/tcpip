# MailBox System
Fulfillment of the requirement

| Requirement     | Implemented?    |
| ----------------|-----------------|
| **IPC**         |                 |
| Pipies/FIFO     |         NO      |
| Message Queue    |         YES      |
| Semaphore    |         YES      |
| ----------------|-----------------|
| **Processes**         |                 |
|  fork()   |         YES      |
|  exec()  |         NO      |
|  signals  |         YES      |
| ----------------|-----------------|
| **Socket**         |                 |
|  TCP   |         YES      |
|  UDP  |         NO      |
| ----------------|-----------------|
| **Socket**         |                 |
|  File Manipulation|         YES(account.txt,unread.txt and read.txt)      |
|  I/O Multiplexing  |         YES      |

Before start the documentation, take note of the tree (directory) structure how it works
```
.
├── client
│   ├── inet.h
│   ├── mailboxClient
│   ├── mailboxClient.c
│   ├── semaphore.h
│   └── sem.h
├── server
    ├── actions.h
    ├── data
    │   ├── account.txt
    │   ├── john_unread.txt
    │   ├── may_read.txt
    │   ├── may_unread.txt
    │   ├── test_read.txt
    │   └── test_unread.txt
    ├── inet.h
    ├── mailboxServer
    └── mailboxServer.c
```

# **Basic codeflow in text description**

* MailboxClient
	* Require access point to server
	* Login()
		* Send a packet which contain **login/<username>;<password>** to server
		* Success -> mainMenu()
		* failed -> back to emailSystem()
	* Register()
		* Send a packet which contain **register/<username>;<password>** to server
		* Success -> back to emailSystem()
		* failed -> received varies of result (e.g. duplicated username,etc), back to email system();
	* mainMenu()
		* Compose() -> send a packet whih contain **compose/<sender>;<receiver>;<content>;<date>;** to server
			* sucess -> back to mainMenu()
			* failed -> received varies of result (e.g. no username avaialble in server, etc), back to system
		* Read() -> send a packet whih contain **<read/unread>/<sender>;<receiver>;<content>;<date>;** to server
			* unread -> display unread message from current user, transfer current unread email to read email, back to mainMenu()
			* read -> display read message from current user, back to mainMenu()
			* failed -> received varies of result (e.g. unable to open file, etc), back to system
	* Additinal functions
		* getDate()
			* email bind with the current date and time created by user
		* display()
			* display read or unread email in structure form
	* Fulfillment based on requirement
		* TCP -> establish TCP connection to server
		* Message Queue -> Implemented in Compose(), this serves to parse a structure of message input by user implemented with fork()
		* Semaphore -> Implemented in main function, once start TCP connection and semaphore will lock and unlock multiple process
		* fork() -> Implemented in main function and compose, main function serves to start the emailSystem() in child process and parent process serves for close the program if found child process end.
		
* MailboxServer
	* Establish a TCP connection waiting any client request to access until the server close
	* Max client in a same time: 5 clients only
	* Once established TCP connection, server wait for client send any command for further action, all of the action are included in action.h
	* Register() (command received: register)
		* Server receive this packet **<username>;<password>** and start process
		* Open account.txt and validate account is duplicated or not
		* Valid -> write into account.txt and reply to client "email sent"
		* Invalid -> reply varies of error (e.g. duplicate username, etc) to client
	* Login() (receive command: login)
		* Server receive this packet **<username>;<password>** and start process
		* open account.txt and validate current available username
		* Valid -> send "logged" to client for letting client access mainMenu()
		* Invalid -> reply varies of error (e.g. failed login, etc) to client
	* ReadMessage() (receive command: unread or read)
		* open X_<unread/read>.txt based on command (X is current user)
		* read
			* open X_read.txt and store the content as buffer send to client for display
		* unread
			* open X_unread.txt and store content as buffer send to client for display
			* transfer X_unread.txt buffer to X_read.txt via remove() unread.txt
		* failed -> display varies of error (e.g. No message found, etc") to client
	* WriteMessage() (receive command: write)
		* Server receive this packet **<sender>;<receiver>;<content>;<date>;** and start process
		* Open X_unread.txt
		* Check availability of client X
		* Success -> continue process
			* Write message into X_unread.txt
			* Send "Email sent successfully!" to client
		* Failed -> send "No receiver client found in server" to client
	* Additional function
		* isExist() -> check the availability of the client by checking the <username> attribute
	* Fulfillment base on requirement
		* TCP -> establish TCP connection and wait any client to access
		* File manipulation -> There are three main Database which is Account, unread and read database in well-structed
		* IO/Multiplexing -> allow multiple client access and preventing same boundary of using same services.
		
# **Notes to write report**

Make sure follow these requirement

* Intro (**No more than 2 page**)
	* Overview (How project solve problem?)
	* Problem Statement (Make sure relate to the objective included from overview)
	* Target user in system
	* System Diagram (Follow the test description will do)
* Background (one page ONLY)
	* LOL LITERATURE REVIEW ON EXISTING TCP EMAIL SYSTEM:( (I will find one)
* Solution/Implementation
	* Limitation (planta-time)
	* Screen-shot 
	* Features (essential part for bonus)
* Appendix
	* Complete Source Code (Lemme do cropping in this, I am using VS Code and it has syntax hightlighting easier to read)